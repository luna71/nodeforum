let createError = require('http-errors');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let logger = require('morgan');
let session = require('client-sessions');
let userCollection = require('./collections/user.js');
let db = require('./src/database.js')

let indexRouter = require('./routes/index');
let usersRouter = require('./routes/users');
let adminRouter = require('./routes/admin');

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
	cookieName: 'session',
	secret: '89muj890mj9d29d0NM(H(*D00M()',
	duration: 1000 * 60 * 60,
}));

function validateUser(req, res, next) {
	if (req.session.user.model != null)
		next();
	else
		res.redirect('/login');
}

function validateAdmin(req, res, next) {
	if (req.session.user.level >= 5)
		next();
	else
		res.redirect('/');
}



app.use((req, res, next)=>{
	if (req.session.user == null) // Not logged in? Give people an empty class
		req.session.user = new userCollection();
	let loggedIn = req.session.user.model == null ? false : true;
	let userData = {
		username: req.session.user.username,
		level: req.session.user.level,
		loggedIn: loggedIn,
	};
	res.locals.currentUrl = req.url;
	res.locals.user = userData;
	next();
})

app.use('/', indexRouter);
app.use('/', validateUser, usersRouter);
app.use('/admin', validateAdmin, adminRouter);


// Only let logged in admins see error logs
app.use(validateAdmin, (req, res, next)=>{
	let err = createError(404);
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	res.status(err.status || 500);
	res.render('error');
})
module.exports = app;
