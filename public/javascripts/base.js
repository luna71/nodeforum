document.addEventListener('DOMContentLoaded', function() {
	var elems = document.querySelectorAll('.sidenav');
	var instances = M.Sidenav.init(elems);

	var modal = document.querySelectorAll('.modal');
    var modalInstances = M.Modal.init(modal);
});