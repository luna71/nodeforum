let mongoose = require('mongoose');
let category = require('./category.js');
let counter = require('./counters.js');
let postCounter = new counter();

let postSchema = mongoose.Schema({
	_id: {type: Number},
	owner: {type: String},
	body: {type: String, unique: false},
	parent: {type: Number, unique: false, default: null},
	date_created: {type: Date, unique: false},
	active: {type: Boolean, unique: false, default: true},
});
let postModel = mongoose.model('post', postSchema, 'posts'); 


class Post {
	constructor() {
		this.baseModel = postModel;
		this.model = null;
	}

	createPost(body, parent, owner){
		return postCounter.getCounter('postCounter')
		.then(()=>{
			return postCounter.getNextValue();
		})
		.then((id)=>{
			let newPost = new postModel({
				_id: id,
				owner: owner,
				body: body,
				parent: parent,
				date_created: new Date(),
			});
			this.model = newPost;
			return newPost.save();
		});
	}

	findPost(name, parent) {
		return new Promise((res, rej)=>{
			postModel.findOne({
				name: name,
				parent: parent,
			}, (err, doc)=>{
				if (doc == null)
					rej();
				this.model = doc;
				res(doc);
			});
		})
		
	}

	deletePost(name, parent) { // Do we need to delete children?
		this.model.active = false;
		return this.model.save();
	}
}

module.exports = Post;