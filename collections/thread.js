let mongoose = require('mongoose');
let category = require('./category.js');
let counter = require('./counters.js');
let posts = require('./posts.js');
let categoryModel = (new category()).baseModel
let postsModel = (new posts()).baseModel

let threadCounter = new counter();

let threadSchema = mongoose.Schema({
	_id: {type: Number},
	name: {type: String, unique: true},
	description: {type: String, unique: false},
	parent: {type: String, unique: false, default: null},
	date_created: {type: Date, unique: false},
	active: {type: Boolean, unique: false, default: true},
});
let threadModel = mongoose.model('thread', threadSchema, 'threads'); 


class Thread {
	constructor() {
		this.baseModel = threadModel;
		this.model = null;
	}

	createThread(name, description, parent){
		return threadCounter.getCounter('threadCounter')
		.then(()=>{
			return threadCounter.getNextValue();
		})
		.then((id)=>{
			let newThread = new threadModel({
				_id: id,
				name: name,
				description: description,
				parent: parent,
				date_created: new Date(),
			});
			this.model = newThread;
			return newThread.save();
		});
	}

	findThread(id) {
		return new Promise((res, rej)=>{
			threadModel.findOne({
				_id: id,
			}, (err, doc)=>{
				if (doc == null)
					rej();
				this.model = doc;
				res(doc);
			});
		})
		
	}

	deleteThread() {
		this.model.active = false;
		return this.model.save();
	}

	getPosts() {
		return new Promise((res, rej)=>{
			postsModel.find({
				parent: this.model._id
			})
			.then((doc)=>{
				res(doc);
			});
		});
	}

	static getThreadsFromParent(parent) {
		return new Promise((res, rej)=>{
			threadModel.find({
				parent: parent
			}, null, {sort: {date_created: -1}},
			(err, doc)=>{
				if (err)
					rej(err);
				res(doc);
			});
		});
	}
}

module.exports = Thread;