let mongoose = require('mongoose');
let counter = require('./counters.js');

let categoryCounter = new counter();


let categorySchema = mongoose.Schema({
	_id: {type: Number},
	name: {type: String, unique: false},
	description: {type: String, unique: false},
	urlname: {type: String, unique: true},
	date_created: {type: Date, unique: false},
	active: {type: Boolean, unique: false, default: true},
});
let categoryModel = mongoose.model('Category', categorySchema, 'categories'); 



class Category {
	constructor() {
		this.baseModel = categoryModel;
		this.model = null;
	}

	createCategory(name, description, urlname){
		return categoryCounter.getCounter('categoryCounter')
		.then(()=>{
			return categoryCounter.getNextValue();
		})
		.then((id)=>{
			let newCategory = new categoryModel({
				_id: id,
				name: name,
				description: description,
				urlname: urlname,
				date_created: new Date(),
			});
			this.model = newCategory;
			return newCategory.save();
		});		
	}

	findCategory(name) {
		return new Promise((res, rej)=>{
			categoryModel.findOne({
				name: name,
			}, (err, doc)=>{
				if (doc == null)
					rej();
				this.model = doc;
				res(doc);
			});
		})
		
	}

	deleteCategory(name) { // Do we need to delete children?
		this.model.active = false;
		return this.model.save();
	}

	static getCategories() {
		return categoryModel.find().exec();
	}
}

module.exports = Category;