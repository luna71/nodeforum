let mongoose = require('mongoose');
let counterSchema = mongoose.Schema({
	_id: String,
	sequence: Number,
});
let counterModel = mongoose.model('counter', counterSchema, 'counters');

class Counter{
	constructor(){
		this.counterModel = null;
	}

	getCounter(name){
		return counterModel.findOne({
			_id: name,
		}).then((doc)=>{
			if (doc == null){
				return Promise.reject();
			}
			this.counterModel = doc;
		})
	}

	getNextValue(){
		if (this.counterModel == null)
			return Promise.reject();
		let value = this.counterModel.sequence;
		this.counterModel.sequence ++;
		return this.counterModel.save()
		.then(()=>{
			return value;	
		});
	}

}

module.exports = Counter;