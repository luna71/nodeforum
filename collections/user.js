var express = require('express');
let mongoose = require('mongoose');
let crypto = require('crypto');

let userSchema = mongoose.Schema({
	username: {type: String, unique: true},
	username_lower: {type: String, unique: true},
	level: {type: Number, default: 0},
	password: {type: String, unique: false},
	salt: {type: String, unique: false},
	date_created: {type: Date, unique:false},
	deleted: {type: Boolean, unique: false, default: false},
});

let userModel = mongoose.model('User', userSchema);

function getHash(password, salt = null) {
	if (!salt){
		salt = crypto.randomBytes(20).toString('hex');
	}
	let hash = crypto.createHash('sha256').update(password + salt).digest('hex');
	return {hash: hash, salt: salt};
}

function getUserByUsername(username) {
	return userModel.findOne({username_lower: username.toLowerCase()}).exec();
}

// Return promises in all methods of User
class User {
	constructor() {
		this.baseModel = userModel;
		this.model = null;

		this.username = null;
		this.level = null;
		this.date_created = null;
		this.deleted = null;
	}

	createUser(username, password) {
		let hash = getHash(password);
		let usernameCheck = getUserByUsername(username);
		let newUser = new userModel({
			username: username,
			username_lower: username.toLowerCase(),
			password: hash.hash,
			salt: hash.salt,
			date_created: new Date()
		});

		return usernameCheck
		.then((doc)=>{ // Check username does not already exist, reject promise.
			if (doc != null)
				return Promise.reject();
		})
		.then(newUser.save());
	}

	deleteUser() {
		this.model.deleted = true;
		return this.model.save();
	}

	loadModel(model) {
		this.model = model;
		this.username = model.username;
		this.level = model.level;
		this.date_created = model.date_created;
		this.deleted = model.deleted;
	}

	findUser(username) { // Usually for administrative purposes this.findUser('username').then(this.deleteUser);
		getUserByUsername(username)
		.then((doc)=>{
			if (doc == null)
				return Promise.reject();
			loadModel(doc);
		})
	}

	loginUser(username, password) {
		let usernameCheck = getUserByUsername(username);
		return usernameCheck
		.then((doc)=>{
			if (doc == null)
				return Promise.reject();
			let hash = getHash(password, doc.salt);
			if (hash.hash != doc.password)
				return Promise.reject();
			this.loadModel(doc);
		});
	}
}

module.exports = User;