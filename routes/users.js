var express = require('express');
var router = express.Router();

let user = require('../collections/user.js');
let category = require('../collections/category.js');
let thread = require('../collections/thread.js');
let post = require('../collections/posts.js');

router.get('/forums', (req,res)=> {
	console.log(req.params['category']);
	category.getCategories()
	.then((categories)=>{
		res.locals.categories = categories;
		res.render('categories');
	})
});

router.get('/forums/:category', (req,res)=> {
	let category = req.params['category'];
	thread.getThreadsFromParent(category)
	.then((docs)=>{
		res.locals.threads = docs;
		res.locals.category = category;
		res.render('threads');
	})
	.catch((err)=>{
		console.log(err);
		res.render('genericError');
	})
});

router.get('/forums/:category/:threadId', (req, res)=>{
	let threadId = req.params['threadId'];
	let currentThread = new thread();
	currentThread.findThread(threadId)
	.then((thread)=>{
		res.locals.thread = thread.name;
		return currentThread.getPosts();
	})
	.then((posts)=>{
		res.locals.posts = posts;
		res.render('thread');
	})
	.catch((err)=>{
		console.log(err);
		res.render('genericError');
	});
});

router.post('/forums/:category/:threadId', (req, res)=>{
	let threadId = req.params['threadId'];
	let body = req.body.body;
	let owner = req.session.user.username;
	let currentPost = new post();
	currentPost.createPost(body, threadId, owner)
	.then(()=>res.redirect(req.url));
});

router.post('/forums/:category', (req, res)=>{
	let user = req.session.user;
	let newThread = new thread();
	let threadName = req.body.title;
	let threadDesc = req.body.body;
	let parent = req.params['category'];
	newThread.createThread(threadName, threadDesc, parent)
	.then((doc)=>{
		res.redirect(req.url);
	})
	.catch(()=>{
		res.render('genericError');
	})
});

module.exports = router;
