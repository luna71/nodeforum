let express = require('express');
let router = express.Router();

let user = require('../collections/user.js');
let category = require('../collections/category.js');
let thread = require('../collections/thread.js');


/* GET home page. */
router.get('/', (req, res)=> res.render('index', { title: 'Materialize Forum' }));
router.get('/about', (req, res)=> res.render('about'));

router.get('/login', (req, res)=> {
	if (req.session.user.loggedIn)
		res.redirect('/');
	let form = {
		title: 'Login',
		method: 'post',
		submit: 'Login',
		inputs: [
			{
				name: 'username',
				label: 'username',
				type: 'text',
			},
			{
				name: 'password',
				label: 'password',
				type: 'password',
			},
		],
	};
	res.render('formGenerator', {form: form});
});

router.get('/logout', (req, res)=>{
	req.session.user = null;
	res.redirect('/');
});

router.get('/register', (req, res)=> {
	if (req.session.user.loggedIn)
		res.redirect('/');
	let form = {
		title: 'Register',
		method: 'post',
		submit: 'Register',
		inputs: [
			{
				name: 'username',
				label: 'username',
				type: 'text',
			},
			{
				name: 'password',
				label: 'password',
				type: 'password',
			},
		],
	};
	res.render('formGenerator', {form: form});
});

router.post('/login', (req, res)=> {
	let username = req.body.username;
	let password = req.body.password;
	if (username.length < 3 && password.length < 5)
		res.redirect('/login');

	let loginUser = new user();
	loginUser.loginUser(username, password)
	.then(()=>{
		req.session.user = loginUser;
		console.log("Hello!");
		res.redirect('/');
	})
	.catch(()=>{
		res.redirect('/login');
	});
});

router.post('/register', (req, res)=> {
	let username = req.body.username;
	let password = req.body.password;
	if (username.length < 3 && password.length < 5)
		res.redirect('/login');

	let loginUser = new user();
	loginUser.createUser(username, password)
	.then(()=>{
		res.redirect('/');
	})
	.catch(()=>{
		res.redirect('/register');
	});
});

module.exports = router;
