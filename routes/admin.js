let express = require('express')
let router = express.Router();

let category = require('../collections/category.js');

router.get('/new/category', (req, res)=>{
	let form = {
		title: 'New Category',
		method: 'post',
		submit: 'Create',
		inputs: [
			{
				name: 'name',
				label: 'name',
				type: 'text',
			},
			{
				name: 'description',
				label: 'description',
				type: 'text',
			},
			{
				name: 'urlname',
				label: 'url name',
				type: 'text',
			},
			{
				name: 'parent',
				label: 'N/A',
				type: 'text',
			},
		],
	};
	res.render('formGenerator', {form: form});
});

router.get('/delete/category', (req, res)=>{
	let form = {
		title: 'Delete category',
		method: 'post',
		submit: 'Delete',
		inputs: [
			{
				name: 'name',
				label: 'category name',
				type: 'text',
			},
			{
				name: 'parent',
				label: 'parent',
				type: 'text',
			},
		],
	};
	res.render('formGenerator', {form: form});
});

router.post('/delete/category', (req, res)=>{
	// TODO
});

router.post('/new/category', (req, res)=>{
	let newCategory = new category();
	let name = req.body.name;
	let description = req.body.description;
	let urlname = req.body.urlname
	let parent = req.body.parent;
	newCategory.createCategory(name, description, urlname, null)
	.then(()=>{
		res.redirect('/forums');
	})
	.catch((err)=>{
		console.log(err);
		res.render('genericError');
	})
});

module.exports = router;